<?php

/**
 * @file
 * Author: Tanay Sai
 * Comment limit - Limit comments by Role
 * Modified from comment_limit module
 * http://drupal.org/project/comment_limit
 */

/**
 * Implements hook_menu().
 */
function comment_limit_by_role_menu() {
  $items['admin/config/content/comment-limit-by-role'] = array(
    'title' => 'Comment Limit By Role Settings',
    'description' => "Settings page for Comment Limit by Role Module",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('comment_limit_by_role_form'),
    'access arguments' => array('administer comment limit by role settings'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'comment_limit_by_role_settings.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function comment_limit_by_role_permission() {
  return array(
    'bypass comment limit' => array(
      'title' => t('Bypass comment limit'),
      'description' => t('Allows a user to bypass the limit set on comments.'),
    ),
    'administer comment limit by role settings' => array(
      'title' => t('Administer Comment Limit By Role Settings'),
      'description' => t('Allows a user to configure Comment Limit by Role Settings'),
    ),
  );
}

/**
 * Implements hook_node_load().
 */
function comment_limit_by_role_node_load($nodes, $types) {
  global $user;
  // Don't check for limits if user has perm to bypass.
  // Or if they can't post comments at all.
  if (user_access('bypass comment limit') || !user_access('post comments')) {
    // This is for the form alter for those who can bypass the limits.
    $_SESSION['commentremaining'] = 'Infinite';
    return;
  }
  foreach ($nodes as $node) {
    $node->comment_limit_by_role = FALSE;
    if (comment_limit_by_role_reached($user)) {
      $node->comment = COMMENT_NODE_CLOSED;
      // Set comment_limit_by_role status.
      $node->comment_limit_by_role = TRUE;
    }
  }
}

/**
 * Implements hook_node_view().
 */
function comment_limit_by_role_node_view($node, $view_mode, $langcode) {
  if (isset($node->comment_limit_by_role) && $node->comment_limit_by_role) {
    if ($node->comment && $view_mode == 'full' &&
        node_is_page($node) && empty($node->in_preview)) {
      $node->content['comments']['comment_form'] = array(
        '#markup' => t('You have reached the comment limit for this content.'),
      );
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function comment_limit_by_role_form_alter(&$form, &$form_state, $form_id) {
  if (strpos($form_id, 'comment_node_') !== FALSE) {
    $form['#prefix'] = t("You can make !commentsremaining more comments.", array('!commentsremaining' => $_SESSION['commentremaining']));
  }
}

/**
 * Get comment count of the user. Comments made in the last x hours.
 *
 * @param int $uid
 *   The userid of the user whose comment count is to be determined
 *
 * @return int
 *   The comment limit for that user
 */
function comment_limit_by_role_count_comments($uid) {

  $count = 0;

  /*
   * Entity field query to get the comments count
   * by the user the the past x hours
   */
  $query = new EntityFieldQuery();
  $yesterday_ts = time() - (((int) variable_get('comment_limit_by_role_for_hours')) * 60 * 60);
  $result = $query
      ->entityCondition('entity_type', 'comment')
      ->propertyCondition('uid', $uid)
      ->propertyCondition('created', "$yesterday_ts", '>=')
      ->execute();

  if (isset($result['comment'])) {
    $count = count($result['comment']);
  }
  return $count;
}

/**
 * Has the limit been reached ?.
 *
 * @param stdobj $user
 *   The user object, infact of the current user,
 *   whose comment limit status is to be checked
 *
 * @return boolean
 *   True if the limit is reached
 */
function comment_limit_by_role_reached($user) {
  $query = db_select('role', 'r');
  $query->fields('r', array('rid', 'name'));
  $result = $query->execute();
  $comment_limit_by_role = 0;
  foreach ($result as $record) {
    $roles_of_user = $user->roles;
    foreach ($roles_of_user as $role_id => $role_name) {
      $roles_comment_limit_by_role = variable_get('comment_limit_by_role_' . $role_id, 0);
      if ($roles_comment_limit_by_role > $comment_limit_by_role) {
        $comment_limit_by_role = $roles_comment_limit_by_role;
      }
    }
  }
  $comment_count = comment_limit_by_role_count_comments($user->uid);
  $_SESSION['commentremaining'] = $comment_limit_by_role - $comment_count;
  return ($comment_limit_by_role != 0 && ($comment_limit_by_role <= $comment_count)) ? TRUE : FALSE;
}
