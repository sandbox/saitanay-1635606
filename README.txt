Comment Limit by Role Module

This module allows  you to restrict the number of comments any user can 
post per day based on his role.

Settings page is at admin/config/content/comment-limit-by-role .

If the user has more than 1 role assigned, then the role having the larger 
limit is considered.

The comment form displays the number of comments he can post. When user 
reaches the limit, the comment form is disabled till the time when he 
can post again.

Credits:
This module borrows code from http://drupal.org/project/comment_limit .

Development was sponsored by www.NoiseFromAmerika.org
