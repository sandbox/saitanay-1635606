<?php

/**
 * @file
 * Author: Tanay Sai
 * Comment limit - Limit comments by Role
 * Modified from comment_limit module
 * http://drupal.org/project/comment_limit
 *
 * Inc file for the settings page.
 */

/**
 * Build the settings form.
 */
function comment_limit_by_role_form() {
  $form = array();
  $query = db_select('role', 'r');
  $query->fields('r', array('rid', 'name'));
  $result = $query->execute();
  foreach ($result as $record) {
    $form['comment_limit_by_role_' . $record->rid] = array(
      '#type' => 'textfield',
      '#title' => check_plain('Daily Limit for ' . $record->name),
      '#size' => 5,
      '#description' => t('Limit per user comments to the given value, e.g. <em>5</em> or <em>10</em>, give <em>0</em> for no limit.'),
      '#default_value' => variable_get('comment_limit_by_role_' . $record->rid, 0),
    );
  }
  $form['comment_limit_by_role_for_hours'] = array(
    '#type' => 'textfield',
    '#title' => check_plain('For every \'n\' hours'),
    '#size' => 5,
    '#description' => t('If this is set to 24, then the limits are per day. To set the limits per hour, enter 1 here.'),
    '#default_value' => variable_get('comment_limit_by_role_for_hours', 24),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Comment Limit settings'),
  );
  return $form;
}

/**
 * Submit handler for settings form.
 */
function comment_limit_by_role_form_submit($form, &$form_state) {
  foreach ($form_state['values'] as $key => $value) {
    variable_set($key, $value);
  }
  variable_set('comment_limit_by_role_for_hours', $form_state['values']['comment_limit_by_role_for_hours']);
  drupal_set_message(t("Message Limit Settings have been Saved"));
}
